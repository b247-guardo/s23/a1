let myTrainer = {
				name: 'Ash Ketchum',
				age: 10,
				pokemon: ['Pikachu', 'Charizard', 'Squritle', 'Bulbasaur'],
				friends: {
					hoenn: ['May', 'Max'],
					kanto: ['Brock', 'Misty'],
				},
				talk: function(){
					console.log('Pikachu! I choose you!');
				}			
			}
			console.log(myTrainer);

			console.log('Result of dot notation:');
			console.log(myTrainer.name);

			console.log('Result of square bracket notation:');
			console.log(myTrainer['pokemon']);

			console.log('Result of talk method:')
			myTrainer.talk();

			function Pokemon(name, level, attack){
				this.name = name;
				this.level = level;
				this.health = 2 * level;
				this.attack = level;

				this.tackle = function(target){
					console.log(this.name + ' tackled ' + target.name);
					console.log(target.name + "'s health is now reduced to " + Number(target.health - this.attack));
					if (target.health <= 0){
						faint();
					}
				}

				this.faint = function(){
					console.log(this.name + ' fainted');
				}

			}

			let Pikachu = new Pokemon('Pikachu', 12, 24, 12);
			console.log(Pikachu);

			let Geodude = new Pokemon('Geodude', 8, 16, 8);
			console.log(Geodude);

			let Mewtwo = new Pokemon('Mewtwo', 100, 200, 100);
			console.log(Mewtwo);

			Geodude.tackle(Pikachu);
			
			let newPikachu = new Pokemon('Pikachu', 12, 16, 12);

			Mewtwo.tackle(Geodude);
			Geodude.faint();

			let newGeodude = new Pokemon('Geodude', 8, -84, 8);

